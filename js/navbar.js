let toggle = document.querySelector('.toggle');
let body = document.querySelector('body');

toggle.addEventListener('click', function() {
    body.classList.toggle('open');
	
})

function disableScroll() {
    document.documentElement.style.overflow = 'hidden';
}

function activateScroll() {
    document.documentElement.style.overflow = '';
}